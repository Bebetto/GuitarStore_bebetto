package com.packt.guitar_store.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


import com.packt.guitar_store.domain.Guitar;
import com.packt.guitar_store.service.GuitarService;
	
	@Controller
	@RequestMapping("/guitars")
	public class GuitarController{
		
		@Autowired
		private GuitarService guitarService;
		
		@RequestMapping
		public String list(Model model){
			model.addAttribute("guitars", guitarService.getAllGuitars());
			model.addAttribute("greeting", "Dean Guitars!");
			model.addAttribute("tagline", "SINCE 76'");
				
		return "guitars";
		
		}
		@RequestMapping("/{type}")
		public String getProductsByCategory(@PathVariable("type") String guitarType, Model model) {
			model.addAttribute("guitars", guitarService.getGuitarsByType(guitarType));
			model.addAttribute("greeting", "Dean Guitars!");
			model.addAttribute("tagline", "SINCE 76'");
			return "guitarsByType";
		}
		
		
		@RequestMapping(value = "/add", method = RequestMethod.GET)
		public String getAddNewGuitarForm(Model model) {
			Guitar newGuitar = new Guitar();
			model.addAttribute("newGuitar", newGuitar);
			model.addAttribute("greeting", "Dean Guitars!");
			model.addAttribute("tagline", "SINCE 76'");
			
		   return "addGuitar";
		}
		   
		@RequestMapping("/guitar")
		public String getProductById(@RequestParam("id") String guitarId, Model model) {
			model.addAttribute("guitar", guitarService.getGuitarById(guitarId));
			model.addAttribute("greeting", "Dean Guitars!");
			model.addAttribute("tagline", "SINCE 76'");
			return "guitar";
		}
		@RequestMapping(value = "/add", method = RequestMethod.POST)
		public String processAddNewGuitarForm(@ModelAttribute("newGuitar") Guitar guitarToBeAdded, Model model, HttpServletRequest request) {
			model.addAttribute("greeting", "Dean Guitars!");
			model.addAttribute("tagline", "SINCE 76'");
			MultipartFile guitarImage = guitarToBeAdded.getGuitarImage();
			String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					
				if (guitarImage!=null && !guitarImage.isEmpty()) {
			       try {
			    	   guitarImage.transferTo(new File(rootDirectory+"resources\\images\\"+guitarToBeAdded.getGuitarId() + ".png"));
			       } catch (Exception e) {
					throw new RuntimeException("Próba zapisu obrazka zakończona niepowodzeniem", e);
			   }
			   }
		   	guitarService.addGuitar(guitarToBeAdded);
			return "redirect:/guitars";
		}
		
		
		
	}


