package com.packt.guitar_store.controller;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.packt.guitar_store.domain.Guitar;


@Controller
public class HomeController {
	
	@RequestMapping(value= "/test",   method = RequestMethod.GET)
	public String welcomeTest() {
		try{
		
	
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager em = emf.createEntityManager();
		
		
		
		System.out.println("udalo sie");
		em.getTransaction().begin();
		System.out.println("udalo sie");
		
		
		em.flush();
		em.getTransaction().commit();
		System.out.println("udalo sie");
		
		String txt = "SELECT g FROM guitars g WHERE g.guitarId='L1002'";
		TypedQuery<Guitar> query = em.createQuery(txt, Guitar.class);
		Guitar guitarra = query.getSingleResult();
		System.out.println(guitarra.getName());
		em.close();
		emf.close();
		System.out.println("udalo sie");
		
		}
		catch(Exception e){
			
		}

		return "redirect:/";
	}


	@RequestMapping("/")
	public String welcome(Model model) {
		model.addAttribute("greeting", "Dean Guitars!");
		model.addAttribute("tagline", "SINCE 76'");
		
		return "welcome";
	}
}
