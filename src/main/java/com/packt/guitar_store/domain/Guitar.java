package com.packt.guitar_store.domain;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;



@Entity(name="guitars")
public class Guitar {
	
	@Id
	@GeneratedValue
	private long serial;
	private String guitarId;
	private String name;
	private String type;
	private int frets;
	private String wood;
	private BigDecimal unitPrice;
	private long unitsInStock;
	private long unitsInOrder;

	@Transient
	private MultipartFile guitarImage;
	
	public Guitar(){
		super();
	}
	public Guitar(String productId, String name, String type, int frets, String wood, BigDecimal unitPrice, long unitsInOrder , long unitsInStock)
	{
		this.frets = frets;
		this.wood = wood;
		this.guitarId = productId;
		this.name = name;
		this.type = type;
		this.unitPrice = unitPrice;
		this.unitsInStock = unitsInStock;
		this.unitsInOrder = unitsInOrder;
	}

	public MultipartFile getGuitarImage() {
		return guitarImage;
	}
	public void setGuitarImage(MultipartFile guitarImage) {
		this.guitarImage = guitarImage;
	}
	
	public long getSerial(){
		return serial;
	}
	public void setSerial(long serial){
		this.serial = serial;
	}
	
	public String getGuitarId() {
		return guitarId;
	}

	public void setGuitartId(String guitarId) {
		this.guitarId = guitarId;
	}

	public int getFrets() {
		return frets;
	}
	public void setFrets(int frets) {
		this.frets = frets;
	}
	public String getWood() {
		return wood;
	}
	public void setWood(String wood) {
		this.wood = wood;
	}
	public void setGuitarId(String guitarId) {
		this.guitarId = guitarId;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public String getType(){
		return type;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public long getUnitsInStock(){
		return unitsInStock;
	}
	
	public void setUnitsInStock(long unitsInStock){
		this.unitsInStock = unitsInStock;
	}
	
	public long getUnitsInOrder(){
		return unitsInOrder;
		
	}
	
	public void setUnitsInOrder(long unitsInOrder){
		this.unitsInOrder = unitsInOrder;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((guitarId == null) ? 0 : guitarId.hashCode());
		return result;
	}

	
}
