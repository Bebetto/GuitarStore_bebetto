package com.packt.guitar_store.domain;

import java.math.BigDecimal;

public class CartItem {

	private Guitar guitar;
	private int quantity;
	private BigDecimal totalPrice;
	
	public CartItem() {
		
	}
	
	public CartItem(Guitar guitar) {
		super();
		this.guitar = guitar;
		this.quantity = 1;
		this.totalPrice = guitar.getUnitPrice();
	}
	
	public Guitar getGuitar() {
		return guitar;
	}
	
	public void setGuitar(Guitar guitar) {
		this.guitar = guitar;
		this.updateTotalPrice();
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
		this.updateTotalPrice();
	}
	
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void updateTotalPrice() {
		totalPrice = this.guitar.getUnitPrice().multiply(new BigDecimal(this.quantity));
	}
	
	@Override
	public int hashCode() {
		final int prime = 311;
		int result = 1;
		result = prime * result + ((guitar == null) ? 0 : guitar.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItem other = (CartItem) obj;
		if (guitar == null) {
			if (other.guitar != null)
				return false;
		} else if (!guitar.equals(other.guitar))
			return false;
		return true;
	}
}
