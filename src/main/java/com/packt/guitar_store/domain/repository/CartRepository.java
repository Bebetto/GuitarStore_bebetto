package com.packt.guitar_store.domain.repository;

import com.packt.guitar_store.domain.Cart;

public interface CartRepository {
	
	Cart create(Cart cart);
	Cart read(String cartId);
	void update(String cartId, Cart cart);
	void delete(String cartId);
		
}
