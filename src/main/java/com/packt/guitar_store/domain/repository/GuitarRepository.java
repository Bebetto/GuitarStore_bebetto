package com.packt.guitar_store.domain.repository;

import com.packt.guitar_store.domain.Guitar;
import java.util.List;


public interface GuitarRepository {

	List <Guitar> getAllGuitars();
	List <Guitar> getGuitarsByType(String type);
	void addGuitar(Guitar guitar);
	Guitar getGuitarById(String guitarId);
	
}
