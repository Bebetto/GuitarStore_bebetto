package com.packt.guitar_store.domain.repository.impl;



import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.packt.guitar_store.domain.Guitar;
import com.packt.guitar_store.domain.repository.GuitarRepository;

@Repository
public class InMemoryGuitarRepository implements GuitarRepository {
	
	private List<Guitar> listOfGuitars;
	
	public InMemoryGuitarRepository()
	{

	}
		private EntityManager entityManagerBegin() {
			
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase", null);
			EntityManager entityManager = entityManagerFactory.createEntityManager();

	
			
			return entityManager;
		
		}
		
		private void entityManagerEnd(EntityManager entityManager) {
			
			System.out.println("Database transactions ended successfull.");
			
			System.out.println("Database connection closing successfull.");
			entityManager.close();
			
			
		}
	
		
		
		
	public List<Guitar> getAllGuitars()
	{
		return listOfGuitars;
	}
	public void addGuitar(Guitar guitar)
	{
	

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager em = emf.createEntityManager();
		
	
		em.getTransaction().begin();
		
		em.persist(guitar);
		em.flush();
		em.getTransaction().commit();
		
		System.out.println("udalo sie");
		em.close();
		emf.close();
	}
	public Guitar getGuitarById(String guitarId) {
		System.out.println("wchodze");
		
		
		

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager em = emf.createEntityManager();
		
		
			
		  String txt = "SELECT g FROM guitars g WHERE g.guitarId='"+ guitarId+"'";
		    TypedQuery<Guitar> query = em.createQuery(txt, Guitar.class);
		    Guitar guitarra  = query.getSingleResult();
	
		

		em.close();
		emf.close();
		
		
		System.out.println();
		return guitarra;

			
	}
	public List<Guitar> getGuitarsByType(String type) {
		
			

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myDatabase");
		EntityManager em = emf.createEntityManager();
		
		
			
		  String txt = "SELECT g FROM guitars g WHERE g.type='"+ type+"'";
		    TypedQuery<Guitar> query = em.createQuery(txt, Guitar.class);
		    List<Guitar> guitarsByCategory = query.getResultList();
	
		

		em.close();
		emf.close();
		
		return guitarsByCategory;
	}


}