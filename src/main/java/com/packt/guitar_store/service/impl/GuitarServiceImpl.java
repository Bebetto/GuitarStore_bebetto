package com.packt.guitar_store.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.guitar_store.domain.Guitar;
import com.packt.guitar_store.domain.repository.GuitarRepository;
import com.packt.guitar_store.service.GuitarService;

@Service
public class GuitarServiceImpl implements GuitarService{
	
	@Autowired
	private GuitarRepository guitarRepository;

	public List<Guitar> getAllGuitars() {
		return guitarRepository.getAllGuitars();
	}

	public Guitar getGuitarById(String guitarID) {
		return guitarRepository.getGuitarById(guitarID);
	}
	public List<Guitar> getGuitarsByType(String type) {
		return guitarRepository.getGuitarsByType(type);
	}
	public void addGuitar(Guitar guitar){
		guitarRepository.addGuitar(guitar);
	}
	
}