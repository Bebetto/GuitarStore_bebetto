package com.packt.guitar_store.service;
import com.packt.guitar_store.domain.Guitar;
import java.util.List;

public interface GuitarService {

	List<Guitar> getAllGuitars();
	List<Guitar> getGuitarsByType(String type);
	Guitar getGuitarById(String guitarID);
	void addGuitar(Guitar guitar);
	
}
