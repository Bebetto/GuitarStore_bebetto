<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		<title>Guitars</title>
	</head>
	<body>
		<section>
			<div class="jumbotron">
				<div class="container">
					<h1> ${greeting} </h1>
					<p> ${tagline} </p>
				</div>
			</div>
		</section>
		<div>
		<section class="container">
		<div class="row" >
			
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Main</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Guitars</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Discounts</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Orders</a></div>
			  
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Contact</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/add" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Add</a></div>
			
		</div>
		</section>
		</div>
		<section class="container"style="padding-top: 40px">
		<div class="row; text-center" >
		
			
			  <c:forEach items="${guitars}" var="guitar">
				<div class="col-sm-2 col-md-12" style="padding-bottom: 15px">
					<div>
						<div class="caption">
						<div class="col-sm-12 col-md-6"><img src="<c:url value="/resource/images/${guitar.guitarId}.png"></c:url>" alt="image"  style = "width:100%"/></div>
							<div class="col-sm-12 col-md-6">
							<h2>${guitar.name}</h2>
							<p>${guitar.unitPrice} $</p>
							<p>
								<a
									href=" <spring:url value="/guitars/guitar?id=${guitar.guitarId}" /> "
									class="btn btn-primary"> <span
									class="glyphicon-info-sign glyphicon" /></span> Details
								</a>
							</p>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>

		
		</div>
		</section>
		
	</body>
</html>
