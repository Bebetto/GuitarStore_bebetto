<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		<title>Witaj</title>
	</head>
	<body>
		<section>
			<div class="jumbotron">
				<div class="container">
					<h1> <center>${greeting} </h1>
					<p> <center>${tagline} </p>
				</div>
			</div>
		</section>
		<div class="thumbnail">
		<section class="container">
		<div class="row">
			
			   <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Main</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Guitars</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="#" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Discounts</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/cart" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Orders</a></div>
			  
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="#" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Contact</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/add" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Add</a></div>
			
		</div>
		</section>
		</div>
		
		<section class="container">
		<div >
		<h1><center>WELCOME TO OUR STORE</h1>
		<h4><center>Check out the guitars and discounts </h4>
		</div>		
		</section>
	</body>
</html>
