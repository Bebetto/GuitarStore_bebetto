<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<script	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.1/angular.min.js"></script>
	<script src="/guitar_store/resource/js/controllers.js"></script>
<title>Guitar</title>
</head>
<body>
<section>
			<div class="jumbotron">
				<div class="container">
					<h1>${guitar.name}  </h1>
					
				</div>
			</div>
		</section>
		<div>
		<section class="container" style="padding-bottom: 50px" >
		<div class="row" >
			
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Main</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Guitars</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Discounts</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Orders</a></div>
			  
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/flying-v" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Contact</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/add" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Add</a></div>
			
		</div>
		</section>
		</div>
	<section class="container" ng-app="cartApp">
		<div class="row">
		<div class="col-md-5">
	<img src="<c:url value="/resource/images/${guitar.guitarId}.png"></c:url>" alt="image"  style = "width:100%"/>
</div>
		
			<div class="col-md-5">
				<h3>${guitar.name}</h3>
				<p>Type: ${guitar.type}</p>
				<p>Wood: ${guitar.wood}</p>
				<p>Frets: ${guitar.frets}</p>
				<p>Price: ${guitar.unitPrice} $</p>
				<p></p>
				<p>Units in Stock: ${guitar.unitsInStock} </p>
				<p>
					<strong>ID: </strong><span class="label label-warning">${guitar.guitarId}</span>
				</p>
				
				<p ng-controller="cartCtrl">
					<a href="#" class="btn btn-warning btn-large" ng-click="addToCart('${guitar.guitarId}')"> 
<span class="glyphicon-shopping-cart glyphicon"></span> Order now </a>
<a href="<spring:url value="/cart" />" class="btn btn-default">
	<span class="glyphicon-hand-right glyphicon"></span> Cart
</a>

 <a href="<spring:url value="/guitars" />" class="btn btn-default">
						<span class="glyphicon-hand-left glyphicon"></span> Get Back
					</a>

				</p>

			</div>
		</div>
	</section>

	</body>
</html>
