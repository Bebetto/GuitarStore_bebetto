<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
		<title>Guitars</title>
	</head>
	<body>
		<section>
			<div class="jumbotron">
				<div class="container">
					<h1> ${greeting} </h1>
					<p> ${tagline} </p>
				</div>
			</div>
		</section>
		<div>
		<section class="container">
		<div class="row" >
			
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Main</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Guitars</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="#" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Discounts</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/cart" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Orders</a></div>
			  
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="#" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Contact</a></div>
			  <div class="col-sm-6 col-md-2" style="padding-bottom: 5px"><c:url value="http://localhost:8080/guitar_store/guitars/add" var="somevar"/>
				<a class="btn btn-default btn-lg active" role="button" href="${somevar}"  >Add</a></div>
			
		</div>
		</section>
		</div>
		<section class="container"style="padding-top: 40px">
		<div class="row; text-center" >
		
			
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px; padding-top: 5px"><div class="thumbnail">
			  <p><c:url value="http://localhost:8080/guitar_store/guitars/les_paul" var="somevar" />
				<a href="${somevar}"><button type="button" class="btn btn-info">LES PAUL</button></a>
				
				</p>
				<p>
				<img src="<c:url value="/resource/images/les_paul.png"></c:url>" alt="image"  style = "width:100%"/>
				</p>
				</div></div>
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px;padding-top: 5px"><div class="thumbnail">
			  <c:url value="http://localhost:8080/guitar_store/guitars/v-flying" var="somevar"/>
				<a href="${somevar}"  ><p><button type="button" class="btn btn-info">FLYING-V</button></p>
				<p>
				<img src="<c:url value="/resource/images/v.png"></c:url>" alt="image"  style = "width:100%"/>
				</p>
				</a></div></div>
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px; padding-top: 5px"><div class="thumbnail">
			  <c:url value="http://localhost:8080/guitar_store/guitars/ml" var="somevar" />
				<a href="${somevar}"><p><button type="button" class="btn btn-info">ML</button></p>
				<p>
				<img src="<c:url value="/resource/images/ml.png"></c:url>" alt="image"  style = "width:100%"/>
				</p>
				</a></div></div>
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px; padding-top: 5px"><div class="thumbnail">
			  <c:url value="http://localhost:8080/guitar_store/guitars/acoustic" var="somevar" />
				<a href="${somevar}"><p><button type="button" class="btn btn-info">ACOUSTIC</button></p></a>
				<p>
				<img src="<c:url value="/resource/images/acoustic.png"></c:url>" alt="image"  style = "width:100%"/>
				</p>
				</div></div>
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px; padding-top: 5px"><div class="thumbnail">
			  <c:url value="http://localhost:8080/guitar_store/guitars/bass" var="somevar" />
				<a href="${somevar}"><p><button type="button" class="btn btn-info">BASS</button></p></a>
				<p>
				<img src="<c:url value="/resource/images/bass.png"></c:url>" alt="image"  style = "width:100%"/>
				</p>
				</div></div>
			  <div class="col-sm-6 col-md-4" style="padding-bottom: 5px; padding-top: 5px"><div class="thumbnail">
			  <c:url value="http://localhost:8080/guitar_store/guitars/ukulele" var="somevar" />
				<a href="${somevar}"><p><button type="button" class="btn btn-info">UKULELE</button></p></a>
				<p>
						<img src="<c:url value="/resource/images/ukulele.png"></c:url>"
							alt="image" style="width: 100%" />
					</p>
				</div>	</div>		  
		
		</div>
		</section>
		
	</body>
</html>
