<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Produkty</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>Produkty</h1>
				<p>Dodaj produkty</p>
			</div>
			<a href="<c:url value="/j_spring_security_logout" />" class="btn btn-danger btn-mini pull-right">wyloguj</a>
		</div>
	</section>
	<section class="container">
			<form:form  modelAttribute="newGuitar" class="form-horizontal" enctype="multipart/form-data">
			<fieldset>
				<legend>Dodaj nowy produkt</legend>

				
				<div class="form-group">
					<label class="control-label col-lg-2" for="guitarId">guitarId</label>
					<div class="col-lg-10">
						<form:input id="guitarId" path="guitarId" type="text" class="form:input-large"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2" for="name">Nazwa</label>
					<div class="col-lg-10">
						<form:input id="name" path="name" type="text" class="form:input-large"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-2" for="type">Type</label>
					<div class="col-lg-10">
						<form:input id="type" path="type" type="text" class="form:input-large"/>
					</div>
				</div>

				
				
				
				<div class="form-group">
					<label class="control-label col-lg-2" for="frets">Number of frets</label>
					<div class="col-lg-10">
						<form:input id="frets" path="frets" type="text" class="form:input-large"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-2" for="wood">Wood</label>
					<div class="col-lg-10">
						<form:input id="wood" path="wood" type="text" class="form:input-large"/>
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="control-label col-lg-2" for="unitPrice">Price</label>
					<div class="col-lg-10">
						<div class="form:input-prepend">
							<form:input id="unitPrice" path="unitPrice" type="text" class="form:input-large"/>
						</div>
					</div>
				</div>
				 
				<div class="form-group">
					<label class="control-label col-lg-2" for="type">Units in order</label>
					<div class="col-lg-10">
						<form:input id="unitsInOrder" path="unitsInOrder" type="text" class="form:input-large"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-2" for="type">Units in stock</label>
					<div class="col-lg-10">
						<form:input id="unitsInStock" path="unitsInStock" type="text" class="form:input-large"/>
					</div>
				</div>
					<div class="form-group">
					<label class="control-label col-lg-2" for="guitarImage">Add Image</label>
					<div class="col-lg-10">
						<form:input id="guitarImage" path="guitarImage" type="file"
							class="form:input-large" />
					</div>
					</div>
				
				
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<input type="submit" id="btnAdd" class="btn btn-primary" value ="Dodaj"/>
					</div>
				</div>
			
			</fieldset>
		</form:form>
	</section>
</body>
</html>
